import reducer from '../../src/reducers/signin'
import { SET_REGISTER_USER_DISPACHER } from '../../src/actions/signin'

test('should set default state', () => {
  const state = reducer(undefined, { type: '@@INIT' });
  expect(state).toEqual({
    accessToken: null,
    email: '',
    username: ''
  });
});

test('fill state of reducer', () => {
  const action = {
    type: SET_REGISTER_USER_DISPACHER,
    payload: {
      accessToken: 'whatever',
      user: {
        email: 'unknown',
        username: 'unknown'
      }
    }
  };

  const state = reducer(undefined, action);
  expect(state).toEqual({
    accessToken: undefined,
    email: 'unknown',
    username: 'unknown'
  });
});