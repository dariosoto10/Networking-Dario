import 'react-native';
import React from 'react';
import Signin from '../../src/components/signin-layout'
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import { mount, shallow } from 'enzyme'
import renderer from 'react-test-renderer';
import { wrap } from 'module';

const IMAGE_PATH_REGEXP = /^..\/..\/..\/src\/asset/

describe('TESTING : LAYOUT <Signin /> ', () => {
  const wrapper = shallow(<Signin />);

  it('render <Signin /> component', () => {
    expect(wrapper).toHaveLength(1);
  })

  describe('TESTING INSTANCE AND CHILDREN OF <Signin /> COMPONENT', () => {
    it('To be equal 1', () => {
      const touchable = wrapper.find('TouchableOpacity')
      expect(touchable).toHaveLength(1)
    })

    it('Have 2 text input', () => {
      const textInput = wrapper.find('TextInput')
      expect(textInput).toHaveLength(2)
    })

    it('Have a header', () => {
      const header = wrapper.find('Header')
      expect(header).toHaveLength(1)
    })

    it('Have two buttons', () => {
      const buttons = wrapper.find('Button')
      expect(buttons).toHaveLength(2)
    })
  })

  describe(`TESTING LAYOUT's TEST`, () => {
    it('To be equal 5', () => {
      const Text = wrapper.find('Text')
      expect(Text).toHaveLength(5)
    })

    it(`The first text to be equal 'Wecolme to Networking!'`, () => {
      const Text = wrapper.find('Text').first()
      expect(Text.props().children).toEqual('Wecolme to Networking!')
    })

    it(`The Last text to be equal '2018 NETWORKING'`, () => {
      const Text = wrapper.find('Text').last()
      expect(Text.props().children).toEqual('2018 NETWORKING')
    })
  })

  describe(`TESTING LAYOUT's STYLES`, () => {
    const Text = wrapper.find('Text')
    
    for (let index = 0; index < Text.length - 1; index++) {
      let _text =  wrapper.find('Text').at(index)
      expect(_text.props().style).toBeTruthy()
    }
  })

  describe(`TESTING LAYOUT's IMAGE TAG`, () => {
    const Image = wrapper.find('Image')

    it('Image should come from asset folder', () => {
      expect(Image.props().source.testUri).toEqual( expect.stringMatching(IMAGE_PATH_REGEXP) )
    })

    it('Image should have a source', () => {
      expect(Image.props().source).toBeTruthy()
    })

    it('Image should have a style', () => {
      expect(Image.props().style).toBeTruthy()
    })
  })
}) 

