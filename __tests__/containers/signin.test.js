import 'react-native';
import React from 'react';
import Signin from '../../src/containers/signin'
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import { mount, shallow } from 'enzyme'
import renderer from 'react-test-renderer';
import { wrap } from 'module';

describe('TESTING : CONTAINER  <Signin />', () => {
  const wrapper = shallow(<Signin />);
  const wrapperInstance = shallow(<Signin />).instance();

  it('render <Signin /> component', () => {
    expect(wrapper).toHaveLength(1);
  })

  describe('TESTING FUNCIONALITY', () => {
    it('Should have a state', () => {
      expect(wrapperInstance.state).toBeTruthy()
    })

    it('Default state should be empty', () => {
      expect(wrapperInstance.state.username).toEqual('')
      expect(wrapperInstance.state.password).toEqual('')
    })
  })
})

