import { put, take } from 'redux-saga/effects';
import { cloneableGenerator } from 'redux-saga/utils';
import { signin, TakeTest } from '../../src/sagas'
import * as ACTIONS from '../../src/actions/signin'
import test from 'tape'

const add = (a, b) => a + b

describe('Should works', () => {
  it('should add two numbers', () => {
    const result = add(3, 4)
    expect(result).toBe(7)
  })
})