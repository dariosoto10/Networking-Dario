import * as ACTIONS from '../../src/actions/signin'

describe('TESTING ACTIONS : ', () => {

  const setRegisterUser = ACTIONS.SET_REGISTER_USER
  it('SET REGISTER USER', () => {
    expect(setRegisterUser).toBe('SET_REGISTER_USER')
  })

  const setRegisterUserDispatch = ACTIONS.SET_REGISTER_USER_DISPACHER
  it('SET REGISTER USER DISPATCHER', () => {
    expect(setRegisterUserDispatch).toBe('SET_REGISTER_USER_DISPACHER')
  })
  
})