import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import Router from './src/Router/index'
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import thunk from 'redux-thunk';
import { Provider } from 'react-redux'
import reducer from './src/reducers'
import rootSaga from './src/sagas'

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  reducer,
  applyMiddleware(sagaMiddleware)
);
export default class App extends Component{
  render() {
    return (
      <Provider store={store}>
        <Router /> 
      </Provider>
    );
  }
}

sagaMiddleware.run(rootSaga)
