import User from '../service/user';

export const SET_SIGNUP_USER = 'SET_SIGNUP_USER'
export const SET_SIGNUP_USER_DISPATCHER = 'SET_SIGNUP_USER_DISPATCHER'
export const SET_SIGNUP_FACEBOOK = 'SET_SIGNUP_FACEBOOK'
export const SET_SIGNUP_FACEBOOK_DISPATCHER = 'SET_SIGNUP_FACEBOOK_DISPATCHER'

export const setSignupUser = payload => ({
  type: 'SET_SIGNUP_USER',
  payload
})

export const setSignupFacebook = payload => ({
  type: 'SET_SIGNUP_FACEBOOK',
  payload
})

export const signInUser = (data) => async (dispatch) => {
  try {
      const result = await User.signInUser(data);
      dispatch(setSignInUser(result.data.data));
  } catch (error) {
      console.log(error.response.data.data.error, 'action');
      return error.response.data.data.error;
  }
};

export const signInUserFacebook = (data) => async (dispatch) => {
  try {
      console.log("Entrando al action Login Facebook");
      const result = await User.signInUserFacebook(data);
      console.log('data Facebook -> ', result.data.data)
  } catch (error) {
      console.log(error);
  }
};
