import Post from '../service/post';

export const setRegisterPost = (payload) => ({
  type: 'SET_REGISTER_POST',
  payload
});

export const registerPost = (data) => async (dispatch) => {
  try {
      const result = await Post.registerPost(data);
      //dispatch(setRegisterPost(true));
      console.log('data -> ', result.data.data);
  } catch (error) {
      console.log(error);
  }
};

