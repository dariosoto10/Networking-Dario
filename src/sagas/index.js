import { call, put, takeEvery, takeLatest, all } from 'redux-saga/effects'
import { SET_SIGNIN_USER } from '../actions/signin'
import * as ACTIONS_SIGNUP from '../actions/signup'
import API from '../service/user'

export function* Signup(action) {
  try {
    const request = yield call(API.register, action.payload)
    yield put({ type: ACTIONS_SIGNUP.SET_SIGNUP_USER_DISPATCHER, payload: request.data.data}) 
  } catch (error) {
    console.log(error)
  }
}

export function* Signin(action) {
  try {
    console.log('enter..')
    const request = yield call(API.signInUser, action.payload)
    yield put({ type: 'SET_SIGNIN_USER_DISPATCHER', payload: request.data.data}) 
  } catch (error) {
    yield put({ type: 'SET_ERROR_DISPATCHER' })
  }
}

export function* watchSigninAsync() {
  yield takeEvery('SET_SIGNIN_USER', Signin)
}

export function* watchSignupAsync() {
  yield takeEvery(ACTIONS_SIGNUP.SET_SIGNUP_USER, Signup)
}

export default function* rootSaga() {
  yield all([
    call(watchSigninAsync), 
    call(watchSignupAsync)
  ])
}