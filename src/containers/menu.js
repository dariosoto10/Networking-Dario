import React, { Component } from 'react';
import MenuLayout from '../components/menu-layout';

export default class Container extends Component {
  constructor(props) {
    super(props);
  }

  postGoTo =() => {
    this.props.navigation.navigate('Post');
  }

  render() {
    return (
      <MenuLayout 
        goTo={this.postGoTo}
        navigation={this.props.navigation}
      />
    );
  }
}
