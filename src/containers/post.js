import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Alert, AsyncStorage } from 'react-native';
import Layout from '../components/post-layout';
import { registerPost } from '../actions/post';

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      category: '',
      message: ''
    };
  }

  handleTitle = () => {
    console.log('title');
  }
  handleCategory = () => {
    console.log('category');
  }
  handleMessage = () => {
    console.log('message');
  }

  handleSumitImage = () => {
    console.log('Subiendo imagen...');
  }

  render() {
    return (
        <Layout
          title={this.handleTitle}
          category={this.handleCategory}
          message={this.handleMessage}
          image={this.handleSumitImage}
        />
    );
  }

}

const mapStateToProps = (state) => ({
  UserDate: state.user
});

const mapDispatchToProps = (dispatch) => ({
  registerPost: (data) => dispatch(registerPost(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Container);
