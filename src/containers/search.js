import React, { Component } from 'react'
import SearchLayout from '../components/search-layout'
import { connect } from 'react-redux'

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: false
    };
  }
  handleToggle = () => {
     this.setState({ toggle: !this.state.toggle });
  }
  
  render() {
    return (
       <SearchLayout 
        toggle={this.state.toggle}
        handleToggle={this.handleToggle}
        navigation={this.props.navigation}
      />
    );
  }

}

const mapStateToProps = (state) => ({
 
});

const mapDispatchToProps = (dispatch) => ({
 
});

export default connect(mapStateToProps, mapDispatchToProps)(Container);
