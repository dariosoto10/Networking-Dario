import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, TextInput, AsyncStorage} from 'react-native'
import HomeLayout from '../components/home-layout'
import { connect } from 'react-redux'

class Home extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return(
      <View>
        <HomeLayout navigation={this.props.navigation}/>
      </View>
    )
  }
}

export default Home
