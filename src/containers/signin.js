import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, TextInput, AsyncStorage} from 'react-native'
import { setSigninUser } from '../actions/signin'
import SigninLayout from '../components/signin-layout'
import { connect } from 'react-redux'
const FBSDK = require('react-native-fbsdk')
const {
  LoginManager,
  AccessToken
} = FBSDK;

class Container extends Component{
  constructor(props) {
    super(props);
    this.state = { 
      username: '',
      password: '',
      accessToken: '',
      token: ''
    };
  }


  componentWillReceiveProps(nextProps) {
    if (nextProps.UserDate.login) {
      const token = nextProps.UserDate.user.token;
      AsyncStorage.setItem('token', token);
      this.props.navigation.navigate('Home'); 
    } else if (nextProps.UserDate.error) {
      alert('password wrong!')
    }
  }

  _requestFacebook = (error, result) => {
    if (error) {
      alert("login has error: " + result.error);
    } 
    else if (result.isCancelled) {
      alert("login is cancelled.");
    } 
    else {
      AccessToken.getCurrentAccessToken().then(
        (data) => {
          //alert(data.accessToken.toString())
          //Actions.home({token: data.accessToken.toString()})
        }
      )
    }
  }

  _loginFacebook = () => {
    LoginManager.logInWithReadPermissions(['public_profile', 'user_friends']).then(
      function(result) {
        if (result.isCancelled) {
          alert('Login cancelled');
        } else {
          AccessToken.getCurrentAccessToken().then(
            (data) => {
              alert(data.accessToken.toString())
              Actions.home()
            }
          )
        }
      },
      function(error) {
        alert('Login fail with error: ' + error);
      }
    );
  }
  _singInGoTo = () => {
    this.props.navigation.navigate('SignUp');
  }

  _handleChangeUsername = event => {
    this.setState({username: event.nativeEvent.text})
  }

  _handleChangePassword = event => {  
    this.setState({password: event.nativeEvent.text})
  }

  _handleSubmit = () => {
    this.props._setSigninUser(this.state) 
  }

  render() {
    return (
      <SigninLayout
        loginFacebookSDK = { this._loginFacebook }
        goTo             = { this._singInGoTo }
        onChangeUsername = { this._handleChangeUsername }
        onChangePassword = { this._handleChangePassword }
        onSubmit         = { this._handleSubmit }
      />
    );
  }
}
const mapStateToProps = state => ({
  UserDate: state.signin
});

const mapDispatchToProps = dispatch => ({
  _setSigninUser: data => dispatch(setSigninUser(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(Container);
