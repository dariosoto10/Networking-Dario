
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TextInput} from 'react-native';
import SignupLayout from '../components/signup-layout'
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { setSignupUser, setSignupFacebook } from '../actions/signup';

 class Container extends Component{
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  _singInGoTo = () => {
    Actions.singin();
  }

  _sumitSingUp = data => {
    this.props.register(data)
  }

  _handleSignupFB = data => { 
    this.props.registerFB(data)
  }

  render() {
    return (
      <SignupLayout
        goTo           = { this._singInGoTo }
        sumitSingUp    = { this._sumitSingUp }
        loading        = { this.state.loading }
        handleSignupFB = { this._handleSignupFB }
      />
    );
  }
}

const mapStateToProps = state => ({
  UserDate: state.signup
});

const mapDispatchToProps = dispatch => ({
  register: data => dispatch(setSignupUser(data)),
  registerFB: data => dispatch(setSignupFacebook(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Container);
