import axios from 'axios';
import { urlBase } from './url';
axios.defaults.baseURL = urlBase;

export default class Post {

  static registerPost(data) {
    return axios.post('/posts', data);
  }
}
