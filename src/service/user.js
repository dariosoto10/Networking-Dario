import axios from 'axios';
import { urlBase } from './url'
axios.defaults.baseURL = urlBase;

export default class User{
  static register(data) {
    return axios.post('/users', data)
  }

  static async signInUser(data){
    return axios.post('/auth', data)
  }

  static signInUserFacebook(token){
    return axios.post('/auth/facebook', token)
  }
}
