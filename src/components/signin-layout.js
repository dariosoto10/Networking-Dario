
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TextInput, Image, Button, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import Header from '../lib/header'
import ModalExample from './modal-layout'

const FBSDK = require('react-native-fbsdk');
const { LoginButton, AccessToken } = FBSDK;

function Layout(props) {
  const { onChangeUsername, onChangePassword, onSubmit, goTo, loginFacebookSDK } = props 
    return (
      <View style={{flex:1}}>
        <Header />
        <View style={styles.title}>
          <Text style={styles.font}>Wecolme to Networking!</Text>
        </View>
        <View style={styles.form}>
          <View>
            <TextInput style={{ color : "white" }}
              onChange={ onChangeUsername }
              placeholder="Email or Username"between component
            />
             <TextInput style={{ color : "white" }}
              onChange={ onChangePassword }
              placeholder="Password"
              secureTextEntry
            />
          </View>
          <View style={styles.row}>
            <Text style={styles.forgot}>Forgot password?</Text>
            <Button style={{marginTop: 40}}
              onPress={ onSubmit }
              title="Login"
              color="#4a1b56"
            />
          </View>
        </View>
        <View>
          <View style={styles.textContainer}>
            <Text style={styles.text}>Don't have an account yet?</Text>
            <Button style={{marginTop: 40}}
              onPress={ goTo }
              title="Sing-in"
              color="#4a1b56"
            />
          </View>
        </View>
        
        <View style={styles.container}>
          <Text style={{marginTop:10}}>Sing-in with your Facebook account</Text>
          <TouchableOpacity onPress = { loginFacebookSDK }>
            <Image 
              source={require('../asset/facebook.png')} 
              style={styles.image} />
          </TouchableOpacity>
        </View>
        
        <View style={styles.footer}>
          <Text>2018 NETWORKING</Text>
        </View>
      </View>
    );
  }

Layout.propTypes = {
  loginFacebookSDK: PropTypes.func.isRequired,
  goTo: PropTypes.func.isRequired,
  onChangeUsername: PropTypes.func.isRequired,
  onChangePassword: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
} 

const styles = StyleSheet.create({
  container: {
    flexDirection : 'row',
    justifyContent: 'space-between',
    padding: 15,
    backgroundColor: 'white'
  },
  header:{
    height: 100,
    backgroundColor: '#7A03E4',
  },
  title:{
    backgroundColor: '#FFFFFF',
    padding: 20
  },
  font:{
    fontSize: 15,
    color: '#8644a5',
    fontFamily: 'Open Sans'
  },
  form:{
    padding: 30,
    backgroundColor: '#8644a5'
  },
  row:{
    flexDirection : 'row',
    justifyContent: 'space-between',
    marginTop: 30
  },
  forgot:{
    color: '#FFFFFF',
    fontFamily: 'Open Sans',
    fontSize: 15
  },
  textContainer:{
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    padding: 10
  },
  text:{
    fontSize: 15,
    marginBottom: 10
  },
  footer:{
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    padding: 10
  },
  image:{
    width: 40,
    height: 40
  }
});

export default Layout

