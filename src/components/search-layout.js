import React, { PureComponent } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';
import Logo from '../asset/logo-small.png';
import Main from '../asset/mainbackground.jpg';
import Menu from '../containers/menu';

export default class Layout extends PureComponent {
  render() {
  return (
    <View >
        <View style={styles.search}>
          <Image source={Logo} />
          <TextInput
          style={{ height: 40, borderColor: 'gray', borderWidth: 1, width: 80 }}
          />
          <Text style={{ marginTop: 5 }}>S</Text>
          <TouchableOpacity onPress={this.props.handleToggle}>
            <Text style={{ marginTop: 5 }}>M</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.header}>
          {this.props.toggle ?
            <Menu navigation={this.props.navigation} /> : <Image source={Main} style={{ width: '100%', height: 230 }} />
          }
        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  search: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
    backgroundColor: 'white',
    height: 50,
  },
  header: {
    height: 80,
  }
});
