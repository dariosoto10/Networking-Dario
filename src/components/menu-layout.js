import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import DropdownMenu from 'react-native-dropdown-menu';

function Layout(props) {
  const { goTo } = props;
  return (
    <View style={styles.container}>
      <View>
        <TouchableOpacity onPress={goTo} style={styles.text} >
          <Text>Feed</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={goTo} style={styles.text} >
          <Text>Post</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={goTo} style={styles.text} >
         <Text>Notifications</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={goTo} style={styles.text} >
          <Text>Profile</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={goTo} style={styles.text} >
          <Text>Log Out</Text>
        </TouchableOpacity>
       </View>
    </View>
    ); 
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      backgroundColor: 'white',
      position: 'absolute',
      width: '100%',
      borderBottomColor: '#4a1b56',
      borderBottomWidth: 30,
      borderTopColor: '#4a1b56',
      borderTopWidth: 1
    },
    text: {
      height: 30,
      color: 'black',
      top: 10,
      alignItems: 'center',
      borderBottomColor: 'black',
      borderBottomWidth: 1,
      marginBottom: 10,
    }
  });
  
  export default Layout
  
