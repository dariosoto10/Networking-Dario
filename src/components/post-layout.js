import React, { Component } from 'react';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { StyleSheet, Text, View, TextInput, Button, TouchableOpacity } from 'react-native';
import Search from '../containers/search';


export default class Layout extends Component {
  render() {
    return (
      <View>
        <Search />
        <View style={styles.title}>
          <Text style={styles.font}>New Post</Text>
        </View>
        <View style={styles.form}>
          <View>
            <TextInput 
              style={{ color: 'black' }}
              onChange={this.props.title}
              placeholder="Title"
            />
             <TextInput 
              style={{ color: 'black' }}
              onChange={this.props.category}
              placeholder="Category"
              secureTextEntry 
             />
             <TextInput 
              multiline
              numberOfLines={4}
              onChange={this.props.message}
             />
          </View>
        </View>
        <View style={styles.option}>
          <TouchableOpacity onPress={this.props.image}>
            <FontAwesome style={{ fontSize: 32 }}>{Icons.camera}</FontAwesome>
          </TouchableOpacity>
          <FontAwesome style={{ fontSize: 32 }}>{Icons.link}</FontAwesome>
          <FontAwesome style={{ fontSize: 32 }}>{Icons.tag}</FontAwesome>
          <Button 
              style={{ marginTop: 40 }}
              title="Post"
              color="#4a1b56"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
    backgroundColor: 'white'
  },
  header: {
    height: 100,
    backgroundColor: '#7A03E4',
  },
  title: {
    backgroundColor: '#4a1b56',
    marginTop: 50,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  font: {
    fontSize: 15,
    color: '#FFFFFF',
    fontFamily: 'Open Sans',
    marginTop: 6
  },
  form: {
    padding: 30,
    backgroundColor: '#FFFFFF'
  },
  option: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
});

