import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TextInput, FlatList, Image, Button, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import Search from '../containers/search'
import body from '../asset/apps.png'
import bodyNew from '../asset/new.png'
import bodyCategories from '../asset/categories.png'

export default function Layout(props) {
  return(
    <View style={styles.container}>
      <Search navigation={props.navigation} />
{/*       <View style={styles.topper} ><Image source={body} /></View>
      <View style={styles.topperNew} ><Image source={bodyNew} /></View>
      <View style={styles.topperCategories} ><Image source={bodyCategories} /></View> */}
      <FlatList style={styles.container}
        data={[{key: 'a'}, {key: 'b'}]}
        renderItem={({item}) => <View>{item.key}</View>}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  topper: {
    marginTop: 300,
    marginLeft: 20
  },
  topperNew: {
    marginTop: 600
  },
  topperCategories: {
    marginTop: 1200 
  }
})