import React, { Component } from 'react';
import { 
  StyleSheet, Text, View, TextInput, Image, Button, TouchableOpacity, Picker , KeyboardAvoidingView
} from 'react-native';
import PropTypes from 'prop-types';
/* import FBSDK from 'react-native-fbsdk'; */
import Background from '../asset/background.jpg';
import Logo from '../asset/logo.png';
import {Spinner} from '../lib/spinner';
import Facebook from '../asset/facebook.png';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
/* import dismissKeyboard from 'dismissKeyboard';
import SmartScrollView from 'react-native-smart-scroll-view'; */

/* const { LoginButton, AccessToken } = FBSDK; */

export default class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      username: '',
      password: '',
      email: '',
      gender: '',
      DD: '',
      MM: '',
      YYYY: ''
    };
  }

  renderingSpinner() {
   if (this.props.loading) {
    return <Spinner />;
   }
   return (
     <Button
       onPress={() => this.props.sumitSingUp(this.state)}
       title="Register"
       color="#4a1b56"
     />
   );
 }
  render() {
    return (
      <View style={{ flex: 1 }}>
       <View style={styles.header}>
          <Image source={Background} style={{ position: 'absolute', height: '100%', width: '100%' }} />
          <Image source={Logo} style={{ left: 35, marginTop: 10 }} />
        </View>
        <View style={styles.title}>
          <Text>Register to be a NetWorking user!</Text>
        </View>
        <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={false}
        >
        <View style={styles.form}>
          <View style={styles.inputText}>  
            <TextInput 
              style={{ color: 'black' }}
              onChangeText={(username) => this.setState({ username })}
              placeholder="Email or Username"
            />
            <TextInput 
              style={{ color: 'black' }}
              onChangeText={(password) => this.setState({ password })}
              placeholder="Password"
              secureTextEntry
            />
            <TextInput 
              style={{ color: 'black' }}
              onChangeText={(email) => this.setState({ email })}
              placeholder="Email"
            />
            <Picker
              selectedValue={this.state.gender}
              onValueChange={(itemValue) => this.setState({ gender: itemValue })}>
              <Picker.Item label="Male" value="Male" />
              <Picker.Item label="Female" value="Female" />
            </Picker><Text>Birthday</Text>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
              <TextInput
                style={{ height: 40, width: 80 }}
                onChangeText={(MM) => this.setState({ MM })}
                placeholder="MM"
              />
                <TextInput
                style={{ height: 40, width: 80 }}
                onChangeText={(DD) => this.setState({ DD })}
                placeholder="DD"
                />
                <TextInput
                style={{ height: 40, width: 100 }}
                onChangeText={(YYYY) => this.setState({ YYYY })}
                placeholder="YYYY"
                />
            </View>
          </View>
          <View style={styles.row}>
            {this.renderingSpinner()}
          </View>
        </View>
        </KeyboardAwareScrollView>
        <View>
          <View style={styles.textContainer}>
            <Text style={styles.text}>Don't have an account yet?</Text>
          </View>
        </View>
        <View style={styles.container}>
          <Text style={{ marginTop: 10 }}>Sing-in with your Facebook account</Text>
          <TouchableOpacity onPress={this.props.loginFacebookSDK}>
            <Image 
              source={Facebook} 
              style={styles.image} 
            />
          </TouchableOpacity>
        </View>
        
        <View style={styles.footer}>
          <Text>2018 NETWORKING</Text>
        </View>
      </View>
    );
  }
}

Layout.propTypes = {
  goTo: PropTypes.func.isRequired,
  sumitSingUp: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: 'white',
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: -10
  },
  header: {
    height: 100,
    backgroundColor: '#7A03E4'
  },
  title: {
    backgroundColor: '#FFFFFF',
    padding: 20
  },
  font: {
    fontSize: 15,
    color: '#8644a5',
    fontFamily: 'Open Sans'
  },
  form: {
    //padding: 30,
    backgroundColor: '#FFFFFF',
    borderStyle: 'solid',
    borderTopColor: '#D8D8D8',
    borderTopWidth: 1,
    borderBottomColor: '#D8D8D8',
    borderBottomWidth: 1,
  },
  inputText: {
    paddingLeft: 20,
    paddingRight: 20,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 40,
    marginBottom: 10
  },
  forgot: {
    color: '#FFFFFF',
    fontFamily: 'Open Sans',
    fontSize: 15
  },
  textContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    padding: 10
  },
  text: {
    fontSize: 15,
    marginBottom: 10
  },
  footer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    padding: 10
  },
  image: {
    width: 40,
    height: 40
  }
});
