import { SET_SIGNUP_USER_DISPATCHER } from '../actions/signup'

const initialState = {
  accessToken: null,
  username: '',
  email:''
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_SIGNUP_USER_DISPATCHER:
      return { ...state, 
        accessToken: action.payload.token,
        username: action.payload.user.username,
        email: action.payload.user.email
      };
      break;
    default:
      return state;
  }
}
