import { combineReducers } from 'redux'
import signup from './signup'
import signin from './signin'

const reducer = combineReducers({
  signup,
  signin
})

export default reducer