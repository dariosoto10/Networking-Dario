const initialState = {
  login: false,
  loading: false,
  user: [],
  error: false
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'SET_SIGNIN_USER_DISPATCHER':
      return { ...state, loading: true, login: true, user: action.payload, error: false };
    case 'SET_ERROR_DISPATCHER':
      return {...state, error: true}
    default:
      return state;
  }
}
