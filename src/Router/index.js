import React, { Component } from 'react'
import { StackNavigator } from 'react-navigation'
import LoginView from '../containers/signin'
import HomeView from '../containers/home'
import SignUp from '../containers/signup'
import PostView from '../containers/post'
import MenuView from '../containers/menu'


const AppRouter = StackNavigator(
  {
    SignIn: { screen : LoginView },
    SignUp: { screen: SignUp },
    Home: { screen: HomeView },
    Post: { screen: PostView },
    Menu: { screen: MenuView }
  },
  {
    navigationOptions: {
      headerStyle: {
        display: 'none',
      }
    },
   
  },
  {
    initialRouteName: 'SignIn',
  }
)
export default AppRouter;