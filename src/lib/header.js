import { StyleSheet, View, Image, Text} from 'react-native';
import React, { Component } from 'react';

export default class Header extends Component {
  render() {
    return(
      <View style={styles.header}>
        <Image source={require('../asset/background.jpg')} style={{position: 'absolute', height: '100%', width: '100%'}} />
        <Image source={require('../asset/logo.png')} style={{left: 35, marginTop: 10}} />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  header:{
    height: 100,
    backgroundColor: '#7A03E4',
  },
  image:{
    width: 40,
    height: 40
  }
});
